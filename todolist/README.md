This is a java application to manage a todo list.

**System Requirement**

JDK 1.8

Maven 3.6.3 or Idea Intellij 2020

**Preparing jar**

`mvn clean install`

This will build the jar file and execute the tests.
If you want to skip the tests, please use

`mvn clean install -DskipTests`

**Running**

_Intellij:_

Right click on TodolistApplication file and select Run 'TodolistApplication'

OR

`mvn exec:java`

**Database**

The application uses H2 db right now. This can be replaced with another database later.

Once the application runs it is available at 'http://localhost:8080'

H2 can be accessed here 'http://localhost:8080/h2-console'. 
Use JDBC url as 'jdbc:h2:mem:testdb' to connect.