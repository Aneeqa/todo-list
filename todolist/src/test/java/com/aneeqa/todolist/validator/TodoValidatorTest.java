package com.aneeqa.todolist.validator;

import com.aneeqa.todolist.domain.TodoItem;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class TodoValidatorTest {

    @InjectMocks
    TodoValidator todoValidator = new TodoValidator();

    static final  String MORE_THAN_MAX = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

    @Test
    public void testValidate() {
        todoValidator.validate(new TodoItem(1, "Task 1", false));
    }

    @Test
    public void testValidate_null_todo() {
        try {
            todoValidator.validate(null);
            Assert.fail("Should have failed");
        } catch (RuntimeException e) {
            Assert.assertEquals("TodoItem cannot be null", e.getMessage());
        }
    }

    @Test
    public void testValidateDescription_null_description() {
        try {
            todoValidator.validate(new TodoItem(1, null , false));
            Assert.fail("Should have failed");
        } catch (RuntimeException e) {
            Assert.assertEquals("Description of a todo item cannot be null or blank", e.getMessage());
        }
    }

    @Test
    public void testValidateDescription_description_greater_than_max() {
        try {
            todoValidator.validate(new TodoItem(1, MORE_THAN_MAX , false));
            Assert.fail("Should have failed");
        } catch (RuntimeException e) {
            Assert.assertEquals("Max length for description is 50 characters", e.getMessage());
        }
    }
}
