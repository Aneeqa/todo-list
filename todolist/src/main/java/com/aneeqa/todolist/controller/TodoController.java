package com.aneeqa.todolist.controller;

import com.aneeqa.todolist.domain.TodoItem;
import com.aneeqa.todolist.service.TodoService;
import java.util.List;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping("/listAll")
    public List<TodoItem> listAll() {
        return todoService.listAll();
    }

    @PostMapping
    public TodoItem add(@RequestBody TodoItem todoItem) {
        return todoService.addTodo(todoItem);
    }

    @PutMapping
    public TodoItem update(@RequestBody TodoItem todoItem) {
        return todoService.update(todoItem);
    }

    @DeleteMapping(value = "{id}")
    public void delete(@PathVariable Integer id) {
        todoService.delete(id);
    }
}
