package com.aneeqa.todolist.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class TodoItem {

  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Id
  private Integer id;

  @Column(nullable = false)
  @NotNull
  private String description;

  @Column(nullable = false)
  @NotNull boolean completed;

  public TodoItem() {}

  public TodoItem(Integer id, String description, boolean completed) {
    this.id = id;
    this.description = description;
    this.completed = completed;
  }

  public TodoItem(String description, boolean completed) {
    this.description = description;
    this.completed = completed;
  }

  public Integer getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isCompleted() {
    return completed;
  }

  public void setCompleted(boolean completed) {
    this.completed = completed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TodoItem todoItem = (TodoItem) o;

    if (completed != todoItem.completed) {
      return false;
    }
    if (id != todoItem.id) {
      return false;
    }
    return description.equals(todoItem.description);
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + description.hashCode();
    result = 31 * result + (completed ? 1 : 0);
    return result;
  }
}
