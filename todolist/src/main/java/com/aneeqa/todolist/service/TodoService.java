package com.aneeqa.todolist.service;

import com.aneeqa.todolist.data.TodoRepository;
import com.aneeqa.todolist.domain.TodoItem;
import com.aneeqa.todolist.validator.TodoValidator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService {

  @Autowired TodoRepository todoRepository;

  @Autowired TodoValidator todoValidator;

  public List<TodoItem> listAll() {
    return todoRepository.findAll();
  }

  public TodoItem addTodo(TodoItem todoItem) {
    todoValidator.validate(todoItem);
    return todoRepository.save(todoItem);
  }

  public TodoItem update(TodoItem todoItem) {
    todoValidator.validate(todoItem);
    TodoItem todo = todoRepository.findById(todoItem.getId()).get();
    if (todo != null) {
      todo.setDescription(todoItem.getDescription());
      todo.setCompleted(todoItem.isCompleted());
      return todoRepository.save(todo);
    } else {
      return null;
    }
  }

  public void delete(Integer id) {
    todoRepository.deleteById(id);
  }
}
