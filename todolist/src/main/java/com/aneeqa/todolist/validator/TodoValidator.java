package com.aneeqa.todolist.validator;

import com.aneeqa.todolist.domain.TodoItem;
import org.h2.util.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TodoValidator {

    public void validate(TodoItem todoItem) {
        if (todoItem == null) {
            throw new RuntimeException("TodoItem cannot be null");
        }
        validateDescription(todoItem.getDescription());
    }

    private void validateDescription(String description) {
        //TODO: Add more validations
        if ( StringUtils.isNullOrEmpty(description)) {
            throw new RuntimeException("Description of a todo item cannot be null or blank");
        }

        if (description.length() > 50) {
            throw new RuntimeException("Max length for description is 50 characters");
        }
    }
}
