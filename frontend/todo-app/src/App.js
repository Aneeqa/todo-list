import React, {Component} from 'react'

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'bootstrap/dist/js/bootstrap.bundle.min'

import TodoApp from "./components/TodoApp"

class App extends Component {

  render() {
    return (
        <div className="App">
          <TodoApp/>
        </div>
    )
  }
}

export default App;
