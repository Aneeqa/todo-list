import React, {Component} from 'react'
import './Header.css'

class Header extends Component {
  render() {
    return (
        <div className="header">
          <div className="center">Todo App</div>
        </div>
    )
  }
}

export default Header