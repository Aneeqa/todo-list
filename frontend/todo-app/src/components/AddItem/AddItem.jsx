import React, {Component, createRef} from 'react';
import './AddItem.css'

class TodoForm extends Component {
  form = createRef();
  itemName = createRef();

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.itemName.current.focus();
  }

  onSubmit(event) {
    event.preventDefault();
    var newItemValue = this.itemName.current.value;

    if (newItemValue) {
      this.props.addItem({newItemValue});
      this.form.current.reset();
    }
  }

  render() {
    return (
        <form ref={this.form} onSubmit={this.onSubmit} className="form-inline">
          <input type="text" ref={this.itemName} className="form-control" maxLength="50"                placeholder="Add task name"/>
          <button type="submit" className="btn btn-info">Add</button>
        </form>
    );
  }
}

export default TodoForm