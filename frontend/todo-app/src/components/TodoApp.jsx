import React, {Component} from 'react'

import Header from './Header/Header'
import AddItem from './AddItem/AddItem'
import Footer from './Footer/Footer'
import Todo from './Todo/Todo'
import TodoService from "../service/TodoService";

class TodoApp extends Component {
  constructor(props) {
    super(props);
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.markTodoDone = this.markTodoDone.bind(this);
    this.state = {todoItems: []};
  }

  componentDidMount() {
    this.getItems();
  }

  getItems() {
    TodoService.retrieveAllTasks()
    .then(response => this.setState({todoItems: response.data}))
  }

  addItem(todoItem) {
    TodoService.addTodo({
      description: todoItem.newItemValue,
      completed: false
    })
    .then(response => {
      this.state.todoItems.push(response.data);
      this.setState({todoItems: this.state.todoItems})
    });
  }

  removeItem(itemIndex) {
    TodoService.deleteTodo(this.state.todoItems[itemIndex]);

    this.state.todoItems.splice(itemIndex, 1);
    this.setState({todoItems: this.state.todoItems});

  }

  markTodoDone(itemIndex) {
    var todo = this.state.todoItems[itemIndex];
    todo.completed = !todo.completed;
    TodoService.updateTodo(todo)
    .then(() => {
    this.setState({todoItems: this.state.todoItems})})

  }

  render() {
    return (
        <div>
          <Header/>

          <div id="main">
            <AddItem addItem={this.addItem}/>
            <Todo items={this.state.todoItems} removeItem={this.removeItem}
                  markTodoDone={this.markTodoDone}/>
          </div>
          <Footer/>
        </div>

    );
  }
}

export default TodoApp
