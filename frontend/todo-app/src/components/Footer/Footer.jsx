import React, { Component } from 'react';
import './Footer.css'

class Footer extends Component {
  render() {
    return (
        <div className="footer">
          &#169; Aneeqa Usmani
        </div>
    )
  }
}

export default Footer