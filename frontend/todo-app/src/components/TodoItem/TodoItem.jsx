import React, {Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './TodoItem.css'
import {faSquareFull} from "@fortawesome/free-solid-svg-icons/faSquareFull";
import {faSquare} from "@fortawesome/free-regular-svg-icons";

class TodoItem extends Component {

  constructor(props) {
    super(props);
    this.onClickClose = this.onClickClose.bind(this);
    this.onClickDone = this.onClickDone.bind(this);
  }

  onClickClose() {
    var index = parseInt(this.props.index);
    this.props.removeItem(index);
  }

  onClickDone() {
    var index = parseInt(this.props.index);
    this.props.markTodoDone(index);
  }

  render () {
    var todoClass = this.props.item.completed ? "done" : "undone";
    return(
        <li className="list-group-item ">
          <div className={todoClass}>
            <FontAwesomeIcon
                icon={todoClass === "done" ? faSquareFull : faSquare}
                className="myicon" onClick={this.onClickDone}/>

            {this.props.item.description}
            <button type="button" className="close"
                    onClick={this.onClickClose}>&times;</button>
          </div>
        </li>
    );
  }
}

export default TodoItem