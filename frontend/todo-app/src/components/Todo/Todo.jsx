import React, {Component} from 'react'
import './Todo.css'
import TodoItem from "../TodoItem/TodoItem";

class Todo extends Component {
  render () {
    var items = this.props.items.map((item, index) => {
      return (
          <TodoItem key={index} item={item} index={index} removeItem={this.props.removeItem} markTodoDone={this.props.markTodoDone} />
      );
    });
    return (
        <ul className="list-group"> {items} </ul>
    );
  }
}

export default Todo