import axios from 'axios'

const API_URL = 'http://localhost:8080'
const LIST_ALL_URL = '/listAll'

class TodoService {
//TODO: Handle errors and exceptions
  retrieveAllTasks() {
    return axios.get(API_URL + LIST_ALL_URL);
  }

  addTodo(todoItem) {
    return axios.post(API_URL, todoItem);
  }

  updateTodo(todoItem) {
    return axios.put(API_URL, todoItem);
  }

  deleteTodo(todoItem) {
    axios.delete(API_URL + '/' + todoItem.id)
  }
}

export default new TodoService()